from falconpy import SensorDownload
from falconpy import APIHarness
from datetime import datetime
import time
import statistics
import hashlib
import os
import csv
import sys
#import json
#import glob
import re


#Grab api keys from Windows Env.
cs_access_key = os.environ.get('cs_access_key')
cs_secret_key = os.environ.get('cs_secret_key')

custId = 'C44B47C79A834FF394B9658404218EB3-E8'
temp_dl_path = 'c:\\temp\\'
cs_name = 'cs_windows_latest.exe'
base_path = r'\\lasfs03\software\Current Versions\CrowdStrike'
local_test = r'c:\temp'
out_file = "cs_version_notes.txt"

dir_win = 'Windows'
dir_sccm = 'SCCM'
dir_mac = 'Mac'
dir_rhel = 'RHEL'
dir_old = 'Old'
dir_test = 'test'

full_Win_path = os.path.join(base_path,dir_win)
full_Mac_path = os.path.join(base_path,dir_mac)
full_RHEL_path = os.path.join(base_path,dir_rhel)
full_sccm_path = os.path.join(base_path,dir_sccm )
re_rh = '^[falcon-sensor]'
re_mac = '^[FalconSensorMacOS]'
re_win = '^[WindowsSensor]'


full_paths = [full_Win_path,full_sccm_path, full_Mac_path, full_RHEL_path]
os_list = ["Amazon Linux","Windows","macOS","RHEL/CentOS/Oracle"]
ver_no = []
temp_ilist = []
final_ilist = []
file_list = []
dir_file = []
fldr_exist = []
today = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
# Clear Screen
os.system('cls')

def get_software(aso):

    # Login to the Falcon API and retrieve our list of sensors
    falcon = SensorDownload(client_id=cs_access_key,
                client_secret=cs_secret_key
                )
    
    #Get list of windows downloads
    response = falcon.get_combined_sensor_installers_by_query(offset=0,limit=3,filter=f"os:'{aso}'",sort=f"version|desc")["body"]["resources"]

    for item in response:
        name = (item["name"])
        os_type = (item["os"])
        ver = (item["version"])
        sha = (item["sha256"])
        rdate = (item["release_date"])
        temp_ilist = [ver,sha,rdate,os_type,name]
        ver_no.append(temp_ilist)

    #Find out which version is the middle version
    mid = statistics.median(ver_no)      
    dl_ver = (mid[0])
    dl_sha = (mid[1])
    dl_rd = (mid[2])

    # Append values to the final list and clearn the ver_no list to grab the next one
    final_ilist = [os_type,name,dl_ver,dl_sha,dl_rd]
    file_list.append(final_ilist)
    ver_no.clear()

    return  file_list

def download_new(dl_sha,dl_path,dl_name):

     # Download with sha256 id

    falcon = SensorDownload(client_id=cs_access_key,
                   client_secret=cs_secret_key
                  )

    dload_response = falcon.download_sensor_installer(id=dl_sha, download_path=dl_path, file_name=dl_name)

    print("Download Completed")

def create_dummy_file(dl_path, dl_name):
    #Create blank file for testing
    dumb_name = (f'{dl_name}_test.txt')
    dumb_path = os.path.join(dl_path,dumb_name)
    print(dumb_path)
    #with open(dumb_path,'w') as  dn:
    #    pass


def check_for_path(folder):  
    # Check if folder exists on drive
    isExist = os.path.exists(folder)
    # Create folder if folder does not exist   
    #print(folder) 
    if isExist != True:
        #print(f'Fold not exist {folder}')
        #print(f'Creating: {folder}')
        #os.mkdir(full_test_path)
        #print(f'Directory {folder} created')
        fldr_exist.append(folder)

    else:
        #print(f'Folder exists: {folder}')
        fldr_exist.append(folder)

    return fldr_exist

def check_for_file(fn):
    #Loop through paths concat with files
    list_dir = os.scandir(fn)  
    
    for entry in list_dir:
        
        if os.path.isfile(entry):
            full_path = entry.path
            if re.match(re_win, entry.name): 
                print(f'Match Win {entry.name} {entry.path}')
                full_file = (f'{entry.path}')
                hash = check_hash(full_file)
                match_version(entry.name, hash,full_Win_path)
            elif re.match(re_rh,entry.name): 
                print(f'Match RH {entry.name} {entry.path}')
                full_file = (f'{entry.path}')
                hash = check_hash(full_file)
                match_version(entry.name, hash,full_RHEL_path)
            elif re.match(re_mac, entry.name): 
                print(f'Match Mac {entry.name} {entry.path}')
                full_file = (f'{entry.path}')
                hash = check_hash(full_file)
                match_version(entry.name, hash,full_Mac_path)
            #else: 
                #print(f'Match other {entry.name} {entry.path}')
                #print("")

def check_hash(file):

    file_name = (file)

    with open(file_name,"rb") as f:
        data = f.read()
        sha256hash = hashlib.sha256(data).hexdigest();
    
    return sha256hash 

def match_version(name,hash,fullp):
    cnt = 0
    total_list = len(file_list) 
    for file in file_list:
        #print(file)
        cnt +=1
        dl_platform = file[0]
        dl_name = file[1]
        dl_ver = file[2]
        dl_sha = file[3]
        dl_path = fullp

        if name == dl_name:
            new_hash =  dl_sha 
            if hash !=  dl_sha:
                #print(f'New  ({dl_sha}) ({dl_path}) ({dl_name}) ({dl_ver})')
                write_ver_file(today,dl_name,dl_ver,dl_path,dl_sha)
                print(f'1 Writing dummy {dl_path}\{dl_name}')
                #create_dummy_file(dl_path,dl_name)
                #download_new(dl_sha,dl_path,dl_name)
            else:
                print(f'Version Current {dl_platform } {dl_path}')
            break
        else:
            if cnt >= total_list:
                #print(f'New ({dl_sha}) ({dl_path}) ({dl_name}) ({dl_ver})')
                #print(f'Log Entry {today} \t {dl_name} \t {dl_name} \t {dl_sha}')
                write_ver_file(today,dl_name,dl_ver,dl_path,dl_sha)
                print(f'2 Writing dummy {dl_path}\{dl_name}')
                #create_dummy_file(dl_path,dl_name)
                #download_new(dl_sha,dl_path,dl_name)
                
def write_ver_file(today,dl_name,dl_ver,dl_sha,dl_path):
    #Function to write out version info into the text files
    full_file = (f'{temp_dl_path}{out_file}')
    log_entry = (f'{today} \t {dl_name} \t {dl_ver} \t {dl_sha} \n')
    #print(f'{today} \t {dl_name} \t {dl_ver} \t {dl_sha}\n')
   

    if os.path.isfile(full_file):
        #Open and append version file
        #print(full_file)
        with open(full_file, 'a') as f:
            f.write(log_entry) 
    else:
        #print('Not Exist')
        #Write version file
        with open(full_file, 'w') as f:
            f.write(log_entry)

#Get File Info from CS

for aso in os_list:
    final_ilist = get_software(aso)

#Ensure path exist, otherwise create it
for path in full_paths:
    check_for_path(path)

#Check for files in network paths
for fn in fldr_exist:
    check_for_file(fn)

